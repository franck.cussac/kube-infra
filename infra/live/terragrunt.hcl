locals {
  # Warning: this name is used in remote_state S3 bucket name. Do not forget to change it if you are copy/pasting this project
  project_name = "hyma-kube"
  region   = "eu-west-1"

  default_yaml_path = find_in_parent_folders("empty.yml")

  ENV = get_env("TF_VAR_ENV", "dev") # "dev" by default to avoid mistakes when developping locally
}

remote_state {
  backend = "s3"
  config = {
    bucket         = "${local.project_name}-terraform-state-management-${local.ENV}"
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.region
    encrypt        = true
    dynamodb_table = "${local.project_name}-${local.ENV}-tfstatelock"
  }
}

# Configure root level variables that all resources can inherit
# We do not use .tfvar files because of this issue: https://github.com/gruntwork-io/terragrunt/issues/873
# Solution: we use .vars.yml files. Under the hood, Terragrunt generates TF_VAR_xxx environment variables from them
# Source: https://github.com/gruntwork-io/terragrunt-infrastructure-live-example/blob/master/prod/terragrunt.hcl
#
# Files are evaluated/overriden into this order (the later takes predecence):
# 0. Inputs defined in this file (main terragrunt.hcl)
# 1. /live/common.vars.yml
# 2. /live/**/common.vars.yml
# 3. /live/<TF_VAR_env>.vars.yml
# 4. /live/**/<TF_VAR_env>.vars.yml
#
inputs = merge(
  {
    project_name : local.project_name,
    region : local.region,
    ENV : local.ENV
  },
  yamldecode(
    fileexists("${find_in_parent_folders("common.vars.yml", local.default_yaml_path)}") ? file("${find_in_parent_folders("common.vars.yml", local.default_yaml_path)}") : "{}",
  ),
  yamldecode(fileexists("${get_terragrunt_dir()}/common.vars.yml") ? file("${get_terragrunt_dir()}/common.vars.yml") : "{}"),
  yamldecode(
    fileexists("${find_in_parent_folders("${local.ENV}.vars.yml", local.default_yaml_path)}") ? file("${find_in_parent_folders("${local.ENV}.vars.yml", local.default_yaml_path)}") : "{}",
  ),
  yamldecode(fileexists("${get_terragrunt_dir()}/${local.ENV}.vars.yml") ? file("${get_terragrunt_dir()}/${local.ENV}.vars.yml") : "{}")
)
