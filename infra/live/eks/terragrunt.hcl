terraform {
  source = "${get_parent_terragrunt_dir()}/../shared-modules//eks" # double slash (//) is required
}

include {
  path = find_in_parent_folders()
}
