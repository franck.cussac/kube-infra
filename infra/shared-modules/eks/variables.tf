variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)
}

variable "tags" {
  description = "The tags to apply to the resources."
  type        = map(string)
}

variable "worker_instance_type" {
  description = "type of the cluster worker instances"
  type        = string
}

variable "worker_root_volume_size" {
  description = "size of workers root volume"
  type        = number
}

variable "worker_asg_desired_capacity" {
  description = "desired capacity for worker auto scalling"
  type        = number
}

variable "worker_asg_max_size" {
  description = "max capacity for worker auto scalling"
  type        = number
}

variable "region" {
  description = "region of the deployement"
  type        = string
}

variable "environment" {
  description = "name of the deployment env, ex: dev or prod"
  type        = string
}

variable "trusted_master_roles" {
  type = list(string)
}

variable "account_id" {
  type = string
}
