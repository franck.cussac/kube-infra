data "aws_iam_policy_document" "assumed_master_role_trust_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = var.trusted_master_roles
    }
  }
}

resource "aws_iam_role" "assumed_master_role" {
  name               = "${var.tags["Name"]}-assumed-master-role"
  assume_role_policy = data.aws_iam_policy_document.assumed_master_role_trust_policy.json
}
