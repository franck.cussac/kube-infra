locals {
  labels = {
    org = "hymaia"
  }
  k8s_tooling_namespace = "kube-system"
  k8s_dns_service_account_name      = "external-dns"
  k8s_external_secrets_service_account_name      = "external-secrets-sa"
  k8s_alb_service_account_name      = "alb-ingress-controller-sa"
  k8s_autoscaller_service_account_name      = "cluster-autoscaler"
  k8s_spark_service_account_name      = "spark-kube"

}

module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = "~> 18.30.0"

  cluster_name                    = var.tags["Name"]
  cluster_version                 = 1.23
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts = "OVERWRITE"
    }
  }

  cluster_encryption_config = [{
    provider_key_arn = aws_kms_key.eks.arn
    resources        = ["secrets"]
  }]

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  # You require a node group to schedule coredns which is critical for running correctly internal DNS.
  # If you want to use only fargate you must follow docs `(Optional) Update CoreDNS`
  # available under https://docs.aws.amazon.com/eks/latest/userguide/fargate-getting-started.html
  eks_managed_node_groups = {
    control-plan = {
      desired_size = 1

      instance_types = ["t3.large"]
      labels = local.labels
      tags = var.tags
    }
  }

  fargate_profiles = {
    argo = {
      name = "argocd"
      selectors = [
        {
          namespace = "argocd"
        },
        {
          namespace = "kube-system"
        }
      ]

      tags = var.tags
      timeouts = {
        create = "20m"
        delete = "20m"
      }
    }

    spark = {
      name = "spark"
      selectors = [
        {
          namespace = "default"
        },
        {
          namespace = "spark"
        }
      ]

      # Using specific subnets instead of the subnets supplied for the cluster itself
      subnet_ids = [module.vpc.private_subnets[1]]
      tags = var.tags
      timeouts = {
        create = "2m"
        delete = "2m"
      }
    }
  }

  tags = var.tags
}

resource "aws_kms_key" "eks" {
  description             = "EKS Secret Encryption Key"
  deletion_window_in_days = 7
  enable_key_rotation     = true

  tags = var.tags
}

module "iam_assumable_role_spark" {
  source       = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version      = "3.6.0"
  create_role  = true
  role_name    = "${var.tags["Name"]}-spark-role"
  provider_url = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns = [
    aws_iam_policy.k8s_spark_access.arn
  ]
  oidc_fully_qualified_subjects = ["system:serviceaccount:spark:${local.k8s_spark_service_account_name}"]
}

resource "aws_iam_policy" "k8s_spark_access" {
  name        = "${var.tags["Name"]}-allow-k8s-spark-access"
  description = "authorize spark to access s3 and KMS to get data and jar"
  policy      = data.aws_iam_policy_document.spark_access.json
}

data "aws_iam_policy_document" "spark_access" {
  statement {
    sid    = "KmsAndS3"
    effect = "Allow"

    actions = [
      "kms:*",
      "s3:*"
    ]

    resources = ["*"]
  }
}
