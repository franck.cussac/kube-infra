#
# policy for alb ingress controller as seen in https://raw.githubusercontent.com/kubernetes-sigs/aws-alb-ingress-controller/v1.1.4/docs/examples/iam-policy.json
#

module "iam_assumable_role_alb" {
  source       = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version      = "3.6.0"
  create_role  = true
  role_name    = "${var.tags["Name"]}-alb-ingress-role"
  provider_url = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns = [
    aws_iam_policy.k8s_alb_access.arn
  ]
  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.k8s_tooling_namespace}:${local.k8s_alb_service_account_name}"]
}

resource "aws_iam_policy" "k8s_alb_access" {
  name        = "${var.tags["Name"]}-allow-k8s-alb-access"
  description = "authorize alb-ingress-controller to access alb to expose k8s service through alb"
  policy      = file("iam_policy/alb_policy.json")
}

resource "helm_release" "alb-ingress-controller" {
  name       = "alb-ingress-controller"
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  namespace = "kube-system"

  set {
    name = "clusterName"
    value = var.tags["Name"]
  }
  set {
    name = "serviceAccount.name"
    value = local.k8s_alb_service_account_name
  }
  set {
    name = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = module.iam_assumable_role_alb.this_iam_role_arn
  }
  set {
    name = "region"
    value = var.region
  }
  depends_on = [module.eks]
}
