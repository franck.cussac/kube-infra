resource "helm_release" "argocd" {
  name       = "argocd"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  namespace = "argocd"
  create_namespace = true

  set {
    name = "version"
    value = "3.6.8"
  }
  depends_on = [module.eks]
}
