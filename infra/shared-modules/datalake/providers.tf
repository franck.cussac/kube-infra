terraform {
  backend "s3" {} # will be filled by Terragrunt
}

terraform {
  required_providers {
    aws = {
      version = "~> 3.22"
    }
  }
}

provider "aws" {
  region = var.region
}
