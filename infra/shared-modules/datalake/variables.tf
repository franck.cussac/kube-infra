variable "tags" {
  description = "The tags to apply to the resources."
  type        = map(string)
}

variable "region" {
  description = "region of the deployement"
  type        = string
}

variable "environment" {
  description = "name of the deployment env, ex: dev or prod"
  type        = string
}
